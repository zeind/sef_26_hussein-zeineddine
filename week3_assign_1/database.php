<?php
class Database{
	//private $dName;

	private $currentDb;
	private $initDir;
	private $currentT;
	private $currentArray; //[0]=> current Db, [1]=>currentT

	function __construct(){
		if(!is_dir("./MadeUpDatabases/")) {
			mkdir("./MadeUpDatabases/");
		}
		chdir("./MadeUpDatabases/");
		if(!is_file("./.currents")){
			$handle=fopen("./.currents", 'w+');
			fclose($handle);
		}
		$this->initDir=getcwd();
		$this->currentArray= file("./.currents", FILE_IGNORE_NEW_LINES);
	}

	function createDb($name){
		chdir($this->initDir);
		try {
			if( is_dir("./$name") ) {
				throw new Exception ("Database $name already exists\n");
			}else {
				mkdir("./$name");
				//$this->currentDb = $name;
				file_put_contents("./.currents", $name);
				echo "New Database $name created!\n";
			}
		}
		catch (Exception $ex ){
			echo $ex->getmessage();
		}
	}

	function setDb($dbName){ //change database of work, kinda like cd
		chdir($this->initDir); //go back to the folder containing all databases
		try{
			if( !is_dir($dbName) ){
				throw new Exception("Database $dbName does not exist\n");
			}else{
				$this->currentDb=$dbName;
				//set current database of work
				file_put_contents("./.currents", $this->currentDb); 
				echo "Now working on database $dbName\n";
			}
		}
		catch (Exception $ex ){
			echo $ex->getmessage();
		}
	} //end of function

	function getDb(){ // get current database of work
		return $this->currentDb;
	}

	function deleteDb($dbName){
		chdir($this->initDir); //go back to the folder containing all databases
		try{
			if( !is_dir($dbName) ){
				throw new Exception("Database $dbName does not exist\n");
			}else{
				array_map('unlink', glob("$dbName/*")	);
				rmdir("$dbName/");
				$this->currentDb = $this->initDir;
				file_put_contents("./.currents", "");
				echo "Database $dbName deleted.\nSELECT a new Database to work on.\n";
			}
		}
		catch (Exception $ex ){
			echo $ex->getmessage();
		}
	}

	function renameDb($prevName, $nextName){
		chdir($this->initDir); //go back to the folder containing all databases
		try{
			if( !is_dir($prevName) ){
				throw new Exception("Database to rename does not exist\n");
			}elseif( is_dir($nextName) ){
				throw new Exception("Database name already exists\n");
			}else{
				rename($prevName, $nextName);
				echo "Database $prevName renamed as $nextName\n";
				file_put_contents("./.currents", $nextName);
			}
		}
		catch (Exception $ex ){
			echo $ex->getmessage();
		}
	}//end of db functions





	// Start with table functions$
	function createT($name, $columns){
		chdir($this->initDir);
		try{
			if( empty($this->currentArray) ){
				throw new Exception("Please specify a database first.\n");
			}
			chdir($this->currentArray[0]);
			if(is_file($name)){
				throw new Exception("Table $name already exists\n");
			}
			else{ //If all is okay, create file and select table
				$handle = fopen($name, 'w+');
				fclose($handle);
				file_put_contents($this->initDir."/.currents", $this->currentArray[0] ."\n".$name);
				$this->initialize($name, $columns);
				echo "Table $name Created!\n";
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}

	function check(){ //Checks if database and table of work are selected
		if(count($this->currentArray) <1 || $this->currentArray[0] == "" ){
			throw new Exception("Please specify database of work\n");
		}
		if(count($this->currentArray) <2 || $this->currentArray[1] == "" ) {
			throw new Exception("Please specify table of work\n");
		}
	}

	function setT($name){
		try{
			if(count($this->currentArray) <1 || $this->currentArray[0] == "" ){
				throw new Exception("Please specify database of work\n");
			}
			chdir($this->initDir."/".$this->currentArray[0]);
			if(!is_file($name)){
				throw new Exception("Table $name does not exist in database ".$this->currentArray[0].".\n");
			}else{
				file_put_contents($this->initDir."/.currents", $this->currentArray[0]."\n".$name);
				echo "Now working on table $name \n";
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}

	function renameT($prevName, $nextName){
		try{
			$this->check();
			chdir($this->initDir."/".$this->currentArray[0]);
			if( !is_file($prevName) ){
					throw new Exception("Table to rename does not exist\n");
			}elseif( is_file($nextName) ){
					throw new Exception("Table name already exists\n");
			}else{
					rename($prevName, $nextName);
					echo "Table $prevName renamed as $nextName\n";
					file_put_contents($this->initDir."/.currents", $this->currentArray[0]."\n".$nextName);
			}
		}
		catch (Exception $ex){
			echo $ex->getmessage();
		}
	}

	function deleteT($name){
		try{
			if(count($this->currentArray) <1 || $this->currentArray[0] == "" ){
				throw new Exception("Please specify database of work\n");
			}
			chdir($this->initDir."/".$this->currentArray[0]);
			if(!is_file($name)){
				throw new Exception("Table does not exist.\n");
			}else{
				unlink($name);
				echo "Table $name deleted.\n";
				file_put_contents($this->initDir."/.currents", $this->currentArray[0]."\n"."" );
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}
	//end of table functions






	//Functions for records start

	function add($id, $atts){
		try{
			$this->check();
			if(!is_numeric($id)){
				throw new Exception("Primary key must be a unique integer\n");
			}
			chdir($this->initDir."/".$this->currentArray[0]);
			$strData = file_get_contents($this->currentArray[1]);
			$arr = unserialize($strData);
			if(count($atts)!=count($arr[0])){
				throw new Exception("Error: For this table you must enter ". count($arr[0]) . " attributes\n");
			}elseif( array_key_exists($id, $arr) ){
				throw new Exception("Primary key $id already exists, try another ID\n");
			}else{
				$row = $atts;
				foreach($row as $key=>$value){ //change column values to have column name
				// as key. example: [name]=>[Hussien]
					$row[$arr[0][$key]] = $row[$key];
					unset($row[$key]);
				}
				$arr[$id]=$row;
				//print_r($arr);
				file_put_contents($this->currentArray[1],serialize($arr));
				echo "Record added to table ".$this->currentArray[1].".\n";
			}
		}
		catch (Exception $ex){
			echo $ex->getmessage();
		}
	}

	function initialize($name, $columns){
		$arr= array($columns);
		$str=serialize($arr);
		file_put_contents($name, $str);
	}

	function deleteR($id){
		try{
			if(!is_numeric($id)){
				throw new Exception("Primary key must be an integer");
			}
			$this->check();
			chdir($this->initDir."/".$this->currentArray[0]);
			$strData = file_get_contents($this->currentArray[1]);
			$arr = unserialize($strData);
			if( !array_key_exists($id, $arr) ){
				throw new Exception("Primary key $id does not exist in this table\n");
			}else{
				echo "Removed record of ID $id\n";
				unset($arr[$id]);
				file_put_contents($this->currentArray[1],serialize($arr));
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}

	function get($input){
		try{
			$this->check();
			chdir($this->initDir. "/" .$this->currentArray[0]);
			$strData = file_get_contents($this->currentArray[1]);
			$arr = unserialize($strData);
			$filteredRows=array();
			foreach($arr as $id=>$row){
				if($id == current($input) && $id!=0 || $this->is_present($row, current($input)) ){
					$filteredRows[$id] = $row;
				} 
			}
			//print_r($filteredRows);
			print("ID | ");
			foreach($arr[0] as $columns){
				print("$columns | ");
			}
			print("\n-----------------------------\n");
			foreach($filteredRows as $key=>$rows){
				print("$key | ");
				foreach($rows as $row){
					print("$row | ");
				}
				print("\n");
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}

	function is_present($row, $str){
		foreach($row as $att){
			if(preg_match("/$str/", $att)){
				return TRUE;
			}
		}
	}

	function show(){
		try{
			if(count($this->currentArray) < 1 || $this->currentArray[0] == '' ){
				throw new Exception("No database selected. SELECT,DATABASE,<NAME>\n");
			}
			print("Working in database {$this->currentArray[0]} \n");
			if(count($this->currentArray) < 2 || $this->currentArray[1] == '' ) {
				throw new Exception("No table selected. SELECT,TABLE,<NAME>\n");
			}else{
				print("Working in Table ".$this->currentArray[1]."\n");
			}
		}
		catch(Exception $ex){
			echo $ex->getmessage();
		}
	}

	//function __destruct(){
	//	chdir($this->initDir);
	//}
}
