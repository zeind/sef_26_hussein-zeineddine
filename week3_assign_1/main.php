#!/usr/bin/php
<?php
require_once("database.php");

while(True){
	
	$initDir = getcwd();
	$db= new Database();
	print("Enter command: \n");
	$line = trim(fgets(STDIN));

	if($line=="EXIT"){
		break;
	}
	
	if(fnmatch("SELECT,DATABASE,*", $line)) {
		$pieces= explode(',', $line);
		$dbName = $pieces[2];
		//print_r("SELECTED DATBASE :" . $dbName."\n");
		$db-> setDb($dbName);
	}

	else if(fnmatch("SELECT,TABLE,*", $line)) {
		$pieces= explode(',', $line);
		$tableName = $pieces[2];
		//print_r("SELECTED TABLE: " . $tableName . "\n");
		$db-> setT($tableName);
	}

	else if(fnmatch("CREATE,DATABASE,*", $line)) {
		$pieces = explode(',', $line);
		$command = array_pop($pieces);
		//print_r($command . "\n");
		$db-> createDb($command); 
	}

	else if(fnmatch("DELETE,DATABASE,*", $line)) {
		$pieces = explode(',', $line);
		$command = array_pop($pieces);
		//print_r($command);
		$db-> deleteDb($command);
	}

	else if(fnmatch("CREATE,TABLE,*,COLUMNS,*", $line)) {
		$pieces = explode(',', $line);
		//print_r($pieces);
		$name=$pieces[2];
		$columns=array_splice($pieces, 4, count($pieces)-4);
		$db-> createT($name, $columns);
	}

	else if(preg_match('/ADD,.+/', $line, $matches)) {
		$pieces = explode(',', $line);
		//print_r($pieces);
		$atts=array_splice($pieces, 2, count($pieces)-2);
		$db-> add($pieces[1], $atts);
	}

	else if(preg_match('/GET,.+/', $line)) {
		$pieces = explode(',', $line);
		$search = array_splice($pieces, 1, count($pieces)-1);
		$db-> get($search);
	}

	else if(preg_match('/DELETE,ROW,.+/', $line)) {
		$pieces = explode(',', $line);
		//print_r($pieces);
		$row=array_pop($pieces);
		$db->deleteR($row);
	}
	elseif(fnmatch("RENAME,DATABASE,*", $line)) {
		$pieces = explode(',', $line);
		$nextName = array_pop($pieces);
		$prevName = array_pop($pieces);
		//print_r($command . "\n");
		$db->renameDb($prevName,$nextName); 
	}
	elseif(fnmatch("RENAME,TABLE,*", $line)) {
		$pieces = explode(',', $line);
		$nextName = array_pop($pieces);
		$prevName = array_pop($pieces);
		//print_r($command . "\n");
		$db->renameT($prevName,$nextName); 
	}
	elseif(fnmatch("SHOW", $line)) {
		$db->show(); 
	}

	else {
		print_r("Not a valid command, please enter a valid command! \n");
	}

	chdir($initDir);

}

	
