
SELECT 
    *#initial.proc_id, COUNT(*)
FROM
    (SELECT
        a.proc_id, a.anest_name, a.start_time, a.end_time
    FROM
        Procedures a
    INNER JOIN Procedures b ON a.anest_name = b.anest_name
    WHERE
        a.start_time <  b.end_time AND a.end_time > b.end_time
            OR a.start_time <= b.start_time AND a.end_time > b.start_time)
    #GROUP BY a.proc_id , a.anest_name , a.start_time , a.end_time)
    AS c
        INNER JOIN
    Procedures AS initial ON c.anest_name = initial.anest_name
        AND initial.start_time <= c.start_time AND initial.end_time > c.start_time
        AND initial.end_time BETWEEN c.start_time AND c.end_time
#GROUP BY initial.proc_id; 