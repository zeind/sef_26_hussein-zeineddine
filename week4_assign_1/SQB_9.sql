Select concat(C.first_name,' ',C.last_name) as customer, count(rental_id) as rentals
	from rental as R
    join customer as C on R.customer_id = C.customer_id
    group by R.customer_id
    order by rentals DESC 
    limit 3;