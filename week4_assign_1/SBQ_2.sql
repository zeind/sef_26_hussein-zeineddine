select L.name as 'language', count(F.language_id) as 'films in that language'
	from language as L
		join film as F 
			on L.language_id = F.language_id
            		and F.release_year = '2006'
	group by L.name 
order by count(F.language_id) Desc
limit 3;
