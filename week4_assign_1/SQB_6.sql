
Select if(isnull(tempBet.name), temp2.name, tempBet.name) as 'category', if(isnull(tempBet.name), temp2.films, tempBet.films) as films
from (select Cat.name, count(FC.film_id) as films
	from category as Cat
		join film_category as FC
			on Cat.category_id = FC.category_id
		group by Cat.name having count(FC.film_id) between 55 and 65
		order by count(FC.film_id) DESC) as tempBet
        right join 
        (select Cat.name, count(FC.film_id) as films
	from category as Cat
		join film_category as FC
			on Cat.category_id = FC.category_id
		group by Cat.name
		order by count(FC.film_id) DESC) as temp2 
        on tempBet.name = temp2.name;
	