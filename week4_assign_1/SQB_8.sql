SELECT 
    S.store_id,
    YEAR(P.payment_date) AS year,
    MONTH(P.payment_date) AS month,
    SUM(P.amount) AS total,
    AVG(P.amount) AS average
FROM
    payment AS P
        JOIN
    staff AS S ON P.staff_id = S.staff_id
GROUP BY S.store_id , YEAR(P.payment_date) , MONTH(P.payment_date)