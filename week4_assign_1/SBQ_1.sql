select concat(A.first_name,' ',A.last_name) as actor_name, count(film_id) as 'num of films'
	from actor as A
		join film_actor as FA on A.actor_id = FA.actor_id
	group by A.actor_id;