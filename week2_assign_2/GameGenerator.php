<?php

	class GameGenerator	{
		private $arrayBig;
		private $arraySmall;
		//private $finalArray;

		function __construct(){
			$this->arrayBig = array(25, 50, 75, 100);
			$this->arraySmall = array(1, 1, 2, 2, 3, 3, 4, 4, 5, 5,
			6, 6, 7, 7, 8, 8, 9, 9, 10, 10);
			$this->finalArray=array();
			//var_dump($arrayBig);
		}

		function getArray(){
			$amountFromBig=rand(1, 4);
			$finalArray = array();
			for($i = 0; $i<$amountFromBig; $i++){
				array_push( $finalArray, $this->arrayBig[array_rand($this->arrayBig)] );
			}

			for($i = 0; $i<6-$amountFromBig; $i++){
				array_push( $finalArray, $this->arraySmall[array_rand($this->arraySmall)] );
			}
			return $finalArray;
		}

		function getTarget(){
			$target = rand(101, 999);
			return $target;
		}


	}