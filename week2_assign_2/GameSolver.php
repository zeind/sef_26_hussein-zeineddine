<?php
	require_once "power_perms.php";

class GameSolver{

	private $finalExp;
	private $finalVar;
	private $tol;
	function __construct(){

	}

	function permute($arr){
		//$this->solve(power_perms($arr), 13);
	}

	function solve($arr, $target){
		$arr= power_perms($arr);
		$this->tol=900;
		$this->finalExp="";
		$this->finalVar;
		$ops= array('+', '*', '-', '/');
		for($i=6; $i <count($arr); $i++) {
			if($i<=35){//if 2 numbers only
				for($j=0; $j<4; $j++){
					$exp= $arr[$i][0].$ops[$j].$arr[$i][1];
					$var=eval('return '.$exp.';' ) ;
					if(is_int($var) && $var>0 && abs($var-$target)<abs($this->tol) ){
						$this->tol = $var-$target;
						$this->finalExp= $exp;
						$this->finalVar=$var;
						if($this->tol==0) return;
					}
				}
			}
			elseif($i<=155){ //if 3 numbers only
				for($j=0; $j<4; $j++){
					for($k=0; $k<4; $k++){
						$exp= $arr[$i][0].$ops[$j].$arr[$i][1].$ops[$k].$arr[$i][2];
						$var=eval('return '.$exp.';' );
						if(is_int($var) && $var>0 && abs($var-$target)<abs($this->tol) ){
							$this->tol = $var-$target;
							$this->finalExp= $exp;
							$this->finalVar=$var;
							if($this->tol==0) return;
						}
					}
				}
			}
			elseif($i<=515){//if 4 numbers only
				for($j=0; $j<4; $j++){
					for($k=0; $k<4; $k++){
						for($l=0; $l<4; $l++){
							$exp= $arr[$i][0].$ops[$j].$arr[$i][1].$ops[$k].$arr[$i][2].$ops[$l].$arr[$i][3];
							$var=eval('return '.$exp.';' );
							if(is_int($var) && $var>0 && abs($var-$target)<abs($this->tol) ){
								$this->tol = $var-$target;
								$this->finalExp= $exp;
								$this->finalVar=$var;
								if($this->tol==0) return;
							}
						}
					}
				}
			}
			elseif($i<=1235){//5 numbers only
				for($j=0; $j<4; $j++){
					for($k=0; $k<4; $k++){
						for($l=0; $l<4; $l++){
							for($m=0; $m<4; $m++){
								$exp= $arr[$i][0].$ops[$j].$arr[$i][1].$ops[$k].$arr[$i][2].$ops[$l].$arr[$i][3].$ops[$m].$arr[$i][4];
								$var=eval('return '.$exp.';' );
								if(is_int($var) && $var>0 && abs($var-$target)<abs($this->tol) ){
									$this->tol = $var-$target;
									$this->finalExp= $exp;
									$this->finalVar=$var;
									if($this->tol==0) return;
								}
							}
						}
					}
				}
			}
			elseif($i<=1955){ //6 numbers only
				for($j=0; $j<4; $j++){
					for($k=0; $k<4; $k++){
						for($l=0; $l<4; $l++){
							for($m=0; $m<4; $m++){
								for($n=0; $n<4; $n++){
									$exp= $arr[$i][0].$ops[$j].$arr[$i][1].$ops[$k].$arr[$i][2].$ops[$l].$arr[$i][3].$ops[$m].$arr[$i][4].$ops[$n].$arr[$i][5];
									$var=eval('return '.$exp.';' );
									if(is_int($var) && $var>0 && abs($var-$target)<abs($this->tol) ){
										$this->tol = $var-$target;
										$this->finalExp= $exp;
										$this->finalVar=$var;
										if($this->tol==0) return;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	function getExp(){
		return $this->finalExp;
	}
	function getVar(){
		return $this->finalVar;
	}
	function getTol(){
		return $this->tol;
	}
}