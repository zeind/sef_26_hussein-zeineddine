<?php
	class GameOutput{

		function __construct(){
		}

		function display($arrNum, $exp, $val, $target, $tol){

			print("{ ");
			foreach($arrNum as $key=> $num){
				if($key<count($arrNum)-1){
					print("$num, ");
				}
				else{
					print("$num");
				}
			}
			print(" }\n");
			print("Target: $target\n");
			if($tol==0){
				$sol = "EXACT";
			}
			else{
				$sol = "Remaining: $tol";
			}
			print("Solution [$sol]: \n");
			print("$exp = $val\n");
			print("-------------\n\n");
		}



	}