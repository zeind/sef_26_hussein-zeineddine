<?php
	require_once "Classes.php";
	require_once "GameSolver.php";
	require_once "GameOutput.php";

	$games=readline("How many games would you like to play?");
	print("\n");

	for($i=1; $i<=$games; $i++){
		$var = new GameGenerator();
		$numbers = $var->getArray();
		//print_r( $numbers );
		$target = $var->getTarget();
		
		$solver = new GameSolver();
		$solver->solve($numbers, $target);
		$expr = $solver->getExp();
		$val = $solver->getVar();
		$tol = $solver->getTol();
		print("Game: $i\n");
		$output = new GameOutput();
		$output->display($numbers, $expr, $val, $target, $tol);
		//print(" Target is: $target.\n Expression is $expr\n value is $val\n tolerance is $tol "); 
	}