#!/usr/bin/php
<?php
	$options=getopt("a:e:t:d:s:m:");
	exec("git log --pretty=format:'%H - %cn - %cd - \"%s\" [%ce %ct]' ", $lines);
	$regex1 = "";
	$regex2 = "";
	$resultsBy = "";
	$andCount=0;
	foreach($options as $key => $searchValue) {
		$index = 0; //double index to avoid blank values
		$array=array(); //reset array
		if ($andCount>0) {
			$resultsBy .= " and ";
		}
		switch ($key) {
			case 'a':
				$resultsBy .= "Author (\"$searchValue\")";
				$regex1 = ".*-.*";
				$regex2 = ".*-.*-";
				break;
			case 't':
				$resultsBy .= "Time ($searchValue)";
				$regex1 = ".* ";
				$regex2 = ":";
				break;
			case 'e':
				$resultsBy .= "Email ($searchValue)";
				$regex1 = "\[";
				$regex2 = "";
				break;
			case 'd':
				$resultsBy .= "Date ($searchValue)";
				$date = DateTime::CreateFromFormat('l-m-Y', $searchValue);
				$date = $date->format('D M .*Y');
				$searchValue = $date;
				$regex1 = "- ";
				$regex2 = " ";
				break;
			case 's':
				$resultsBy .= "Timestamp ($searchValue)";
				$regex1 = "\[.*";
				$regex2 = "";
				break;
			case 'm':
				$resultsBy .= "Commit message ($searchValue)";
				$regex1 = "\".*";
				$regex2 = ".*\"";

				break;
		}
		foreach($lines as $line){
			$regex= "$regex1($searchValue)$regex2";
			//print("$regex \n");
			preg_match("/$regex/", $line, $matches);
			if(sizeof($matches)>0) {
				$array[$index] = $line;
				$index++;
			}
		}
		$andCount++;
		$lines = $array;
	}

	print("Results by $resultsBy - Total: ". sizeof($array)."\n");
	foreach($array as $filtered){
		$filtered = substr($filtered, 0, strpos($filtered,'['));
		print("$filtered \n");
	}