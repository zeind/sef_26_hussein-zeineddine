#!/usr/bin/php
<?php 
	$lines=file("/var/log/apache2/access.log");
	//To put each line of the file as array value
	$exp='/(^\S+) \S+ \S+ \[([^+:]+)\:(\S+) \S+ (\"[^"]+\") (\d+)/';
	//To search, group, and capture specific expressions
	foreach($lines as $line){
		preg_match($exp, $line, $g);
		//This puts each captured expression in its variable
		$ip = $g[1];
		$date = $g[2];
		$time = str_replace(':','-',$g[3]);
		$type = $g[4];
		$code = $g[5];
		$timestamp=DateTime::CreateFromFormat('d/M/Y', $date);
		//Encodes date to a format understood by the computer
		$fdate = $timestamp->format('l, F jS Y');
		//Decodes date to display day of week, month etc..
		echo "$ip -- $fdate : $time -- $type -- $code\n";
	}
?>