#!/usr/bin/php
<?php 

	$path = "$argv[2]";
	echo "Files within $path: \n";
	files($path);

	function files($path) {
		$objects=scandir($path);
		foreach($objects as $object){
			if($object != "." && $object != ".."){ //skips . and .. files
				if(!is_dir($path."/".$object)) echo "$object \n";
				else{
					files($path."/".$object); // Recursive layer portal, just like Inception
					//All that we see or seem is but a dream within a dream - E.A.P
				}
			}
		}
	}
?>
