@extends('layouts.app')

@section('content')
	@foreach($posts as $post)
	<div class="wrapper">	
		
		<div class="card">
		  <a href="comments/{{$post['id']}}">
		  	<img class="card-img-top" src="{{ url('../storage/app/' . $post['path']) }}" alt="Card image cap">
		  </a>
		  <div class="card-block">
		    <p class="card-text">{{$post['caption']}}</p>
		  </div>
		</div>
	</div>
	@endforeach
@endsection
