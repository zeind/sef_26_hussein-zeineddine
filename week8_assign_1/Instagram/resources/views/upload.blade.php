@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(array('url' => 'upload','enctype' => 'multipart/form-data')) !!}
    <div class="row cancel">
        <div class="col-md-5">
            {!! Form::file('image', array('class' => 'image')) !!}
        </div>
        <div class="col-md-5">
            <button type="submit" class="btn btn-success">Create</button>
        </div>
        <div class="col-md-6">
            {{ Form::label('caption:') }}
            <textarea name='caption' class="form-control" rows='3'></textarea>
        </div>
    </div>
{!! Form::close() !!}

@endsection