<?php

namespace Instagram;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [

    	'user_id',
    	'caption',
    	'path'

    ];

    public function user()
    {
        return $this->belongsTo('Instagram\User');
    }

    public function comments()
    {
        return $this->hasMany('Instagram\Comment');
    }

    public function likes()
    {
        return $this->hasMany('Instagram\Post');
    }
}

