<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;
use \Instagram\Comment;
use \Instagram\Post;

class CommentController extends Controller
{
    public function show($id){
		// $id = Session::get('id');
		// $arr = Post::select('id', 'path','caption','created_at')->where('user_id', $id)->orderBy('created_at', 'desc')->get();
		// $arr = Comment::select('content', 'post_id', 'user_id')
		// Comment::->post()->select('path')

		$post = Post::find($id)->select('id', 'path', 'user_id')->get()->toArray();
		$content = Post::find($id)->comments()->select('content')->orderBy('created_at', 'desc')->get()->toArray();
		// dd($content);

		return view('comments')->with('post', $post)->with('content', $content);
	}
}
