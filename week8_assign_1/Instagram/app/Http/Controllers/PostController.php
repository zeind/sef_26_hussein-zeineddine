<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;

use Instagram\Post;

use Auth;
use Session;

class PostController extends Controller
{


	// public function __construct()
 //    {
 //        $this->middleware('auth');
 //    }


	public function index()
	{
		

		return view('upload');
	}

	/**
	 * Shows the profile of the userID passed to it in the url
	 * @return [type] [description]
	 */
	public function show($id){
		// $id = Session::get('id');
		$arr = Post::select('id', 'path','caption','created_at')->where('user_id', $id)->orderBy('created_at', 'desc')->get()->toArray();

		return view('profile')->with('posts', $arr);
	}


	/**
	 * Stores an image in database and path
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function fileUpload(Request $request)
    {
    	$user = Auth::user();
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
        //number 
        $num = Post::where('user_id', $user->id)->max('id');
        if( is_null($num) ){
        	$num = 0;
        }

        //retrieve image info
        $cap = $request->input('caption');
        $ext = $request->file('image')->getClientOriginalExtension();
        $path = $request->file('image')->storeAs('images/'.$user->id, round(microtime(true) * 1000) .'.'.$ext);

        //fill the post and push ii to database
        $post = new Post;
        
        $post->caption = $cap;
        $post->path = $path;
        $post->user_id = $user->id;
        $post->save();


        return redirect('profile/' .$user->id);
    }

}
