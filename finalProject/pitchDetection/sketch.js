//Input variables
let mic;
let fft;
let spectrum;
let fftBins = 1024;
let fftSize = fftBins*2;
let rate = 44100;
let threshold = 160;



//Processing and output variables
let midiNote;
let letterNote = 'C';
let DoReNote = 'Do';
let harmonics = [];
let initialFundamental;
let notes = ['C', 'C#',
             'D', 'D#',
             'E',
             'F', 'F#',
             'G', 'G#',
             'A', 'A#',
             'B'];

let doReNotes = [ 'Do', 'Do#',
                  'Re', 'Re#',
                  'Mi',
                  'Fa', 'Fa#',
                  'Sol','Sol#',
                  'La', 'La#',
                  'Si'];

//Display variables
let isDoRe = false;                  
let circle;
let button;
let startButton;
let started = false;

function setup() {
  let canvas = createCanvas(windowWidth, windowHeight);

  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT(0, fftBins);
  fft.setInput(mic);

  
  circle = new Circle(width/2, height/2, 250);

  // Create Do Re Mi button
  createDoReButton();

  // Create Start Button
  createStartButton();
  
  textSize(64);
  textAlign(CENTER);
}


function draw() {
	  background(140);
	if (started) { 
	  stroke(255);
	  spectrum = fft.analyze();
	  process(spectrum); 
	  circle.update();
	}
	else {
		// Just wait for a click
	}
}


/**
 * Starts it
 * @return {[type]} [description]
 */
function start() {
	started = true;
	startButton.remove();
	button.show();
}

/**
 * Processes the frequency spectrum and zero pads it for better efficiency
 * @param  {array} spec frequency spectrum
 * @return {[type]}      [description]
 */
function process(spec) {

  harmonics = [];
  for (let i = 1; i < spec.length - 500; i++) {
    if ( spec[i] > threshold ) {

      if (spec[i] > spec[i-1] && spec[i] > spec[i+1]) {

        if ( isHarmonic(i, spec) ) {
          circle.trigger(spec[i]);
          initialFundamental = i * rate/fftSize;
          break;
        }
        else {
          spec[i] = 0;
        }

      }
      else {
        spec[i] = 0;
      }

    }
    else {
      spec[i] = 0;
    }

  }

}



/**
 * Checks if the specified index is indeed a musical harmonic note
 * in a specific depth and around a radius or error
 * @param  {int}  index peak index
 * @param  {array}  spec  frequency spectrum
 * @return {Boolean}       
 */
function isHarmonic(index, spec) {
  // In a small range, check if there is a peak at integer multiples, for depth harmonics
  let depth = 4;
  let harm = 0;
  let offset = 0;
  // How many harmonics, depth
  for (let i = 2; i< depth+2; i++) {
    // Radius of error to check for peak
    for( let j = -2; j < 3; j++) {
      offset = index*i + j;
      if( spec[offset] >= spec[offset - 1] && spec[offset] >= spec[offset + 1] ) {
        harm ++;
        harmonics.push(offset * rate/fftSize);
        break;
      }
    }
  }
  if (harm == depth) {
    refineFundamental();
    return true;
  }
  else {
    harmonics = [];
    return false;
  }
}


/**
 * Refines the fundamental frequency using its harmonics
 * @return {[type]} [description]
 */
function refineFundamental() {
  let sum = initialFundamental;
  for (let i = 1; i < harmonics.length-1; i++) {
    sum += harmonics[i]-harmonics[i-1];
  }
  avg = sum/(harmonics.length-1);
  getLetterNote(avg);
}


/**
 * Retrieves the note in Letter form
 * @param  {float} freq the refined frequency
 */
function getLetterNote(freq) {
  midiNote = freqToMidi(freq)%12;
  letterNote = notes[midiNote];
  DoReNote = doReNotes[midiNote];
}


/**
 * Toggle the Do Re Mi button to C D E
 * @return {[type]} [description]
 */
function toggleDoRe() {
  if(isDoRe) {
    button.html('Do Re Mi');
    isDoRe = false;
  }else{
    button.html('C D E');
    isDoRe = true;
  }
}


/**
 * Circle class
 * @param {int} x   coordinate
 * @param {int} y   coordinate
 * @param {int} min minimum size
 */
Circle = function (x, y, min) {
  this.x = x;
  this.y = y;
  this.min = min;
  this.change = 0;
}


Circle.prototype.update = function() {
  fill(0, 140, 140);
  stroke(10);
  ellipse(this.x, this.y, this.min + this.change, this.min + this.change);
  fill(30);
  if(isDoRe) {
    text(''+DoReNote, this.x, this.y + 20);
  }else{
    text(''+letterNote, this.x, this.y + 20);
  }
  this.change *= 0.98;
};


Circle.prototype.trigger = function(amp) {
  this.change = amp;
};



function createDoReButton() {
	button = createButton('Do Re Mi');
  button.size(200, 100);
  button.position(100, height/2 - 50);
  button.style('background', '#008c8c');
  button.style('color', 'black');
  button.style('border', '1px solid black');
  button.style('font-size', '18pt');
  button.style('cursor', 'pointer');
  button.style('border-radius', '50%');
  button.style('outline', 'none');
  button.mouseClicked(toggleDoRe);
  button.hide();
}

function createStartButton() {
	startButton = createButton('Start');
  startButton.position(width/2 - 150, height/2 - 50);
  startButton.size(300, 100);
  startButton.style('background', '#8c3c8c');
  startButton.style('border-radius', '5%');
	startButton.style('color', 'black');
  startButton.style('border', '1px solid black');
  startButton.style('cursor', 'pointer');

  startButton.mouseClicked(start);
  
}