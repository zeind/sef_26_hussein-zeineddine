var mic, fft;
var inc = 0;

function setup() {
   createCanvas(710,400);
   noFill();

   mic = new p5.AudioIn();
   mic.start();
   fft = new p5.FFT();
   fft.setInput(mic);
}

function draw() {
   background(200);

   var spectrum = fft.analyze();

   var cmpSpec = compress(spectrum);

   beginShape();
   for (i = 0; i<cmpSpec.length; i++) {
    var amp = cmpSpec[i];
    var y = map(amp, 0, 10000, height, 0);
    vertex(i, y );
   }
   endShape();

  if( inc == 500) { 
  	console.log(typeof spectrum);
  }
  inc ++;

}

function compress(spec) {
	var cmpSpec = spec;
	for ( var cmp = 2; cmp < 4; cmp ++) {
		for( var i = 1; i < cmpSpec.length; i++) {
			cmpSpec[i] = cmpSpec[i] * compressedSample(spec, 1, cmp, i);
		}
	}
	return cmpSpec;
}

function compressedSample(spec, offset, cmp, loc) {
	if (offset + loc * cmp < spec.length) {
		return spec[offset + loc*cmp];
	}
	return 0;
}
// function toggle() {
// 	if (song.isPlaying() ){
// 		song.pause();
// 		button.html('Play');
// 	}
// 	else{
// 		button.html('Pause');
// 		song.play();
// 	}
// }