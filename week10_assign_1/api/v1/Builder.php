<?php
class Builder {


	/**
     * Builds the GET query to be sent to the wrapper
     * @return String The query as a string
     */
    public function get()
    {   
        $inputs = $_GET;
        if( array_key_exists('table', $inputs) ){
            array_shift($inputs);
            $queryStr = 'SELECT * FROM ' . $inputs['table'];
            array_shift($inputs);
            $params = array();
            foreach ($inputs as $key => $value) {
                $params[] = $key . ' = "' . $value . '"';
            }
            $filters = ' WHERE ' . implode(' AND ', $params);
            return $queryStr . $filters;
        }
        else {
            throw new Exception("Please specify a table");
        }
    }	

    /**
     * Builds the POST query to be send to the wrapper
     * @return [String] 
     */
    public function post()
    { 
    	$allowedTables = array('customer', 'address', 'film', 'actor');
    	$inputs = $_POST;
    	if ( array_key_exists('table', $inputs) ) {
    		if ( in_array($inputs['table'], $allowedTables) ){
	    		$queryStr = 'INSERT INTO ' . $inputs['table'];
	    		array_shift($inputs);
	    		$columns = array();
	    		$values = array();
	    		foreach ($inputs as $key => $value) {
	    			$columns[] = $key;
	    			$values[] = '"' . $value . '"';
	    		}
	    		$atts = ' (' . implode(', ', $columns) . ')';
	    		$vals = ' VALUES (' . implode(', ', $values) . ')';
	    		$fullQuery = $queryStr . $atts . $vals;
	    		return $fullQuery;
	    	}
	    	else {
	    		throw new Exception("Specified table not accessible, please check priviliges and spelling");
	    	}
    	}
    	else {
            throw new Exception("Please specify a table.");
    	}
    }


    public function update()
    {
    	$allowedTables = array('customer',
						'address',
						'film', 
						'actor',
						'store',
						'rental',
						'payment',
						'staff'
						);
    	$data = $_POST;
    	$cond = $_GET;

    	if ( array_key_exists('table', $cond) ) {
    		if ( in_array($cond['table'], $allowedTables) ) {
	    		array_shift($cond);
	    		$queryStr = 'UPDATE ' . $cond['table'] . ' SET ';
	    		array_shift($cond);
	    		$attVal = array();
	    		foreach ($data as $key => $value) {
	    			$attVal[] = $key .' = "' . $value . '"';
	    		}
	    		$atts = implode(', ', $attVal);

	    		foreach ($cond as $key => $value) {
	    			$params[] = $key . ' = "' . $value . '"';
	    		}

            	$filters = ' WHERE ' . implode(' AND ', $params);

	    		$fullQuery = $queryStr . $atts . $filters;
	    		return $fullQuery;
	    	}
	    	else {
	    		throw new Exception(
	    			"Specified table not accessible, please check priviliges and spelling"
	    		);
	    	}
    	}
    	else {
            throw new Exception("Please specify a table.");
    	}
    }

    public function delete()
    {
    	$allowedTables = array('customer', 'address', 'film', 'actor');
    	$inputs = $_GET;
    	if ( array_key_exists('table', $inputs) ) {
	        if( array_key_exists('table', $inputs) ) {
	            array_shift($inputs);
	            $queryStr = 'DELETE FROM ' . $inputs['table'];
	            array_shift($inputs);
	            $params = array();
	            foreach ($inputs as $key => $value) {
	                $params[] = $key . ' = "' . $value . '"';
	            }
	            $filters = ' WHERE ' . implode(' AND ', $params);
	            return ($queryStr . $filters);
	        }
	        else {
	            throw new Exception("Please specify a table");
	        }
		}
		else {
			throw new Exception(
	    			"Specified table not accessible, please check priviliges and spelling"
	    		);
		}	 
    }

}