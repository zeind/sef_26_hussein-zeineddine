<?php
require_once 'API.php';
require_once 'MySQLWrapper.php';
require_once 'config.php';
require_once 'Builder.php';

class MyAPI extends API
{
    protected $User;

    public function __construct($request, $origin) {
        parent::__construct($request);
    }

    /**
     * gets the results of the query from the wrapper
     * @return string JSON representation of the results
     */
    protected function read()
    {
        if ($this->method == 'GET') {
            $db = new database( DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE );
            $builder = new Builder();
            $queryStr = $builder->get();
            if( $db->query($queryStr) ) {
                return print(json_encode($db->results(), JSON_PRETTY_PRINT));
            }
            else { 
                throw new Exception( $db->error() );
            }
        }
        else {
            throw new Exception("The 'read' endpoint only accepts GET requests");
        }
    }

    protected function create()
    {
        if ($this->method == 'POST') {
            $db = new database( DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE );
            $builder = new Builder();
            $queryStr = $builder->post();
            if( $db->query($queryStr) ) {
                return print("Successfully Done: " . $queryStr);
            } else { 
                throw new Exception( $db->error() );
            }
        }
        else{
            throw new Exception("The 'create' endpoint only accepts POST requests");
        }
    }

    /**
     * [update description]
     * @return [type] [description]
     */
    protected function update()
    {
        if ($this->method == 'POST') {
            $db = new database( DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE );
            $builder = new Builder();
            $queryStr = $builder->update();
            if( $db->query($queryStr) ) {
                return print("Successfully Done: " . $queryStr);
            } else { 
                throw new Exception( $db->error() );
            }
        }
        else{
            throw new Exception("The 'update' endpoint only accepts POST requests");
        }
    }

    protected function delete()
    {
        if ($this->method == 'DELETE') {
            $db = new database( DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE );
            $builder = new Builder();
            $queryStr = $builder->delete();
            if( $db->query($queryStr) ) {
                return print("Successfully Done: " . $queryStr);
            } else { 
                throw new Exception('Operation Unsuccessful '. $db->error() );
            }
        }
        else{
            throw new Exception("The 'update' endpoint only accepts POST requests");
        }   
    }

 }
